The idea of reaching a goal with as little resources as possible. For an overview of minimalism in computing, see [Wikipedia](https://en.wikipedia.org/wiki/Minimalism_(computing)).
