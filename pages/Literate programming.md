An approach to program design and documentation that embeds software source code into an explanatory narrative, structuring the code to follow the narrative for convenience of a human reader.

Further reading:
- [Literate programming](https://doi.org/10.1093/comjnl/27.2.97) by D. Knuth. The paper proposing the idea and describing the first supporting tools.
