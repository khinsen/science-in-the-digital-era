[🌱](Seedling.md)

In 1973, Ivan Illich published a book entitled "[Tools for conviviality](https://en.wikipedia.org/wiki/Tools_for_Conviviality)", in which he argues for *convivial* technology, which he defines as technology that empowers people to do what they want, rather than constraining their choices. An important aspect of convivial technology is that people must be able to adapt it to their needs and wishes.

Convivial software is thus software that its users are not only allowed to modify (that would be [Open Source](Open%20Source.md)), but which is designed and written with the explicit goal of facilitating modification by its users. For now, it is vision rather than an established practice. Modifying software always requires a higher technical competence than merely using it. Convivial software should both reduce the barrier between user and developer and provide guidance for the user motivated to overcome it. Ideas for reaching this ideal include [explainable](Making%20Systems%20Explainable.md) systems, [malleable](Malleable%20Systems.md) systems, [re-editable](Reusable%20vs.%20re-editable%20components.md) software, and the general quest for [simplicity](In%20search%20of%20simplicity.md).

Further reading:
 - [Convivial Computing](https://damaged.bleu255.com/Convivial_Computing/) in the [Damaged Earth Catalog](https://damaged.bleu255.com/)
 - [Convivial design heuristics for software systems](https://doi.org/10.1145/3397537.3397543), by [Stephen Kell](https://www.humprog.org/~stephen/)

