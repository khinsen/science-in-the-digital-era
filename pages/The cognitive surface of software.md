[🌱](Seedling.md)

I have been trying for a while to come up with a good term for the concept I will describe here, and for now I have settled on "cognitive surface". It may not be the best one. If you have an idea for a better metaphor, please get in touch!

In analogy with physical objects, I call the parts of a computation that are readily comprehensible by its users its *cognitive surface*, and the parts that are comprehensible only with specialist knowledge, specialist tools, or with significant effort, its *cognitive bulk*.

Note that comprehensible is not the same as visible. For Open Source software, the source code is visible, but most of it doesn't make sense to a user with reasonable effort, and therefore belongs to the bulk. What is and what isn't comprehensible depends on the person approaching the computation. It is not an objective property of the computation or software, but a relational property involving a person and an artifact. But then, this is true for the accessible surface of physical objects as well. A tunnel with a small opening is accessible to a child, but not to an adult.

In the context of standard command-line or GUI applications, the cognitive surface of a computation consists of the software's user interface, its documentation, and of inputs and outputs of an execution. Techniques such as [literate programming](Literate%20programming.md), [computational notebooks](Computational%20notebook.md), or [moldable development](Moldable%20development.md) aim at increasing the cognitive surface by attaching explanations to (parts of) the code. Some notebook implementations, and [Glamorous Toolkit](Glamorous%20Toolkit.md) as the primary implementation of [Moldable Development](Moldable%20development.md), also add tools for interactive exploration of algorithms and data, be they inputs, outputs, or intermediate results. The conference talk [Making Systems Explainable](Making%20Systems%20Explainable.md) gives a good illustration.

Increasing the cognitive surface is one way of reducing the [epistemic opacity](Epistemic%20opacity.md) of a computation, increasing [trust](Trustworthy%20software.md) in its correctness.

Another technique for reducing epistemic opacity is decreasing the cognitive bulk. One approach is reducing the amount of code, leading to [minimalism](Minimalism.md). A complementary approach is reducing the functionality of software, by removing what is not required for a specific use case. This leads to [re-editable code](Reusable%20vs.%20re-editable%20components.md) and [situated software](Situated%20software.md).
