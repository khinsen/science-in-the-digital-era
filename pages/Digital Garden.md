[🌿](Plant.md)

A digital garden is a small ecosystem of interrelated documents that its curator tends to with regular updates and revisions. It differs from a blog, which is a stream of finished-then-published documents. A digital garden can be considered a special case of a Wiki that is curated by a single person or a small team, in contrast to open-to-all collaborative works such as Wikipedia.

Some recent entries start with an icon indicating its level of maturity, an idea taken from [Maggie Appleton's Digital Garden](https://maggieappleton.com/garden):

- 🌱 [Seedling](Seedling.md): New ideas and concepts that may or may not turn out to be useful.
- 🌿 [Plant](Plant.md): Entries that have proven useful and are linked to from other parts of the garden. The text has room for improvement and is likely to evolve.
- 🌲 [Evergreen](Evergreen.md): Notes or essays that are fully worked out. They may receive minor updates or correction from time to time but are otherwise stable.

These categories are very much 🌱 as I just started to use them, so they may well evolve. Note that many entries do not have a maturity indicator, because they mostly serve as keywords for cross-referencing, or as anchors that provide links to external resources.

Recommended reading on digital gardens:

 - [The Garden and the Stream: A Technopastoral](https://hapgood.us/2015/10/17/the-garden-and-the-stream-a-technopastoral/amp/) by Mike Caulfield
 - [A Brief History & Ethos of the Digital Garden](https://maggieappleton.com/garden-history) by Maggie Appleton
 
