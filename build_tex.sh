python3 pages2chapters.py all.md
pandoc -f markdown+header_attributes -t latex all.md -o all.tex -s \
   --top-level-division=chapter \
   -V colorlinks=true \
   -V linkcolor=blue \
   -V urlcolor=magenta \
   --template=template.tex
mkdir -p temp
mv all.tex temp/temp.tex
cd temp
cat temp.tex \
   | sed s/🌱/\\\\emoji{seedling}/g \
   | sed s/🌿/\\\\emoji{potted-plant}/g \
   | sed s/🌲/\\\\emoji{evergreen-tree}/g \
   > all.tex
lualatex all.tex
lualatex all.tex
